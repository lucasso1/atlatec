#include "matrix.h"
#include "vector.h"

using namespace atlatec;


int main() {

	auto m1 = Matrix<double>(2, 3);
    auto m2 = Matrix<double>(3, 5);

    m1.setValue(0,0,3);
	m1.setValue(0,1,5);
	m1.setValue(0,2,-1.5);
	m1.setValue(1,0,4);
	m1.setValue(1,1,6);
	m1.setValue(1,2,-2.5);

	m2.setValue(0,0,2);
	m2.setValue(0,2,4);
	m2.setValue(1,1,-10);
	m2.setValue(1,3,-11);
	m2.setValue(2,2,0.5);
	m2.setValue(2,4,7);

    std::cout << "M1:\n" << m1;
    std::cout << "M2:\n" << m2;

    auto m3 = m1 * m2;

    std::cout << "M3:\n" << m3;
	Matrix<double> m3dot2 = m3;
	m3dot2 *= 2;
	std::cout << "M3*2:\n" << m3dot2;
	m3dot2 += m3;
	std::cout << "M3*2 + M3:\n" << m3dot2;
	m3 *= 0;
	std::cout << "M3*0:\n" << m3;

	auto v1 = Vector<double>(2);
	v1.setValue(0, 0.5);
	v1.setValue(1, -0.5);

	std::cout << "V1:\n" << v1;
	std::cout << "V1*M3*2:\n" << v1*m3dot2;

	v1+=v1;
	std::cout << "V+V:\n" << v1;
	v1*=0.1;
	std::cout << "(V+V)/10:\n" << v1;

    return 0;
}
