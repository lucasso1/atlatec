#ifndef ATLATEC_VECTOR_H
#define ATLATEC_VECTOR_H

#include "matrix.h"

namespace atlatec
{

template <class ValueT> class Vector;
template <class ValueT> std::ostream & operator<<(std::ostream & os, Vector<ValueT> const &);

// it is a row vector
template <class ValueT>
class Vector
{
	Matrix<ValueT> matrix;

public:

	Vector(uint len) : matrix(1, len) {}
	Vector(Matrix<ValueT> && other) : matrix(std::move(other))
	{
		assert(matrix.getRows() == 1);
	}

	void setValue(uint idx, ValueT val)
	{
		this->matrix.setValue(0, idx, val);
	}

	ValueT getValue(uint idx, ValueT) const
	{
		return this->matrix.getValue(0, idx);
	}

	Vector & operator*=(ValueT val) {
		matrix *= val;
		return *this;
	}

	Vector & operator+=(Vector const & other) {
		matrix += other.matrix;
		return *this;
	}

	operator Matrix<ValueT> const &() const { return this->matrix; };

	friend std::ostream & operator<<<>(std::ostream & os, Vector const &);
};

template <class ValueT> inline std::ostream & operator<<(std::ostream & os, Vector<ValueT> const & vector) {
	return os << "Vec:" << vector.matrix;
}

template <class ValueT> inline Vector<ValueT> operator*(Vector<ValueT> const & v, Matrix<ValueT> const & m) {
	return static_cast<Matrix<ValueT>>(v)*m;
}

template <class ValueT> inline Matrix<ValueT> operator*(Matrix<ValueT> const & m, Vector<ValueT> const & v) {
	return m*static_cast<Matrix<ValueT>>(v);
}

template <class ValueT> inline Vector<ValueT> operator*(Vector<ValueT> const & v1, Vector<ValueT> const & v2) {
	return static_cast<Matrix<ValueT>>(v1)*static_cast<Matrix<ValueT>>(v2);
}

}

#endif //ATLATEC_VECTOR_H
