#ifndef ATLATEC_MATRIX_H
#define ATLATEC_MATRIX_H
#include <iostream>
#include <vector>
#include <cassert>

namespace atlatec
{

using uint = unsigned int;

template <class ValueT> class Matrix;
template <class ValueT> std::ostream & operator<<(std::ostream & os, Matrix<ValueT> const & matrix);
template <class ValueT> Matrix<ValueT> operator*(Matrix<ValueT> const & m1, Matrix<ValueT> const & m2);

template <class ValueT> class Vector;

template <typename ValueT>
class Matrix
{
	uint const rows;
	uint const cols;
	std::vector<ValueT> data;
	ValueT scalar;

	uint computeDataIdx(uint cellRow, uint cellColumn) const {
		assert(cellRow < rows);
		assert(cellColumn < cols);
		return cellRow * cols + cellColumn;
	}

	friend std::ostream & operator<<<>(std::ostream & os, Matrix const &);
	friend Matrix operator*<>(Matrix const &, Matrix const &);

public:
	Matrix(uint rows, uint cols) :
			rows(rows), cols(cols), data(rows * cols, 0), scalar(1)
	{
		assert(rows>0); assert(cols>0);
	}

	void setValue(uint cellRow, uint cellColumn, ValueT val)
	{
		assert(scalar == 1); // setValue can be used during initialization only
		data[computeDataIdx(cellRow,cellColumn)] = val;
	}

	ValueT getValue(uint i, uint j, ValueT val) const
	{
		return data[computeDataIdx(i,j)] * scalar;
	}

	uint getRows() const { return rows; }
	uint getCols() const { return cols; }

	Matrix & operator*=(ValueT s) {
		this->scalar *= s;
		return *this;
	}

	Matrix & operator+=(Matrix const & other) {
		assert(rows == other.rows && cols == other.cols);
		auto itThis = std::begin(data);
		auto itOther = std::begin(other.data);

		while (itThis != std::end(data)) {
			*itThis = *itThis * scalar + *itOther * other.scalar;
			++itThis; ++itOther;
		}

		scalar = 1;

		return *this;
	}
};

}

#endif //ATLATEC_MATRIX_H
