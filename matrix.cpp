#include "matrix.h"
#include "vector.h"

namespace atlatec
{

template <class ValueT> std::ostream & operator<<(std::ostream & os, Matrix<ValueT> const & matrix)
{
	uint i = 0;
	for (auto val : matrix.data)
	{
		os << val*matrix.scalar;
		if (++i==matrix.cols)
		{
			os << std::endl;
			i=0;
		}
		else
		{
			os << ",";
		}
	}
	return os;
}

// operator* below was designed to show on job interview to show c++ skills
// If I would have more free time or if you expect me to speed up algorithm more I would implement https://en.wikipedia.org/wiki/Strassen_algorithm maybe with threads also.
// if I would have to write it in real product I would use internally functions from OpenBLAS: https://github.com/xianyi/OpenBLAS, definitely not implementing by
// hands core oerations, just C++ wrappers and classes.

template <class ValueT>
Matrix<ValueT> operator*(Matrix<ValueT> const & m1, Matrix<ValueT> const & m2)
{
	assert(m1.cols == m2.rows);
	auto result = Matrix<ValueT>(m1.rows, m2.cols);
	result.scalar = m1.scalar * m2.scalar;
	if (result.scalar == 0) return result;

	for (uint resRow = 0; resRow < result.rows; ++ resRow)
	{
		for (uint resCol = 0; resCol < result.cols; ++ resCol)
		{
			ValueT & cell = result.data[result.computeDataIdx(resRow, resCol)];
			uint m1idx = m1.computeDataIdx(resRow, 0);
			uint m2idx = m2.computeDataIdx(0, resCol);
			for (uint i = 0; i < m1.cols; ++i)
			{
				cell += m1.data[m1idx] * m2.data[m2idx];
				++m1idx; m2idx += m2.cols;
			}
		}
	}

	return result;
}

template std::ostream & operator<<<double>(std::ostream &, Matrix<double> const &);
template Matrix<double> operator*<double>(Matrix<double> const &, Matrix<double> const &);

}