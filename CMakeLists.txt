cmake_minimum_required(VERSION 3.14)
project(atlatec)

set(CMAKE_CXX_STANDARD 17)

add_executable(atlatec main.cpp matrix.cpp matrix.h vector.h)